#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/PointCloud.h>
#include <signal.h>
#include <math.h>


ros::Publisher pub_cmd_vel;
geometry_msgs::Twist msg;

void sensor_callback(const sensor_msgs::PointCloud::ConstPtr &pointcloud)
{

    float linear = 0.2;
    float angular = 0;
    float turn_p = 0.80;
    float turn_p2 = 0.65;
    float turn_p3 = 0.40;
    float turn_f = 1;
    float turn_f2 = 4;

    float a = 1;
    float b = 2;

if ((pointcloud->points[0].y < turn_f) && (-pointcloud->points[7].y > turn_f2)) angular = 0.3;
if ((pointcloud->points[0].y > turn_f2) && (-pointcloud->points[7].y < turn_f)) angular = -0.3;

    for (int i = 1; i< 7; i++)
    {
    float x = pointcloud->points[i].x/a;
    float y = pointcloud->points[i].y/b;
    float r = sqrt(x*x + y*y);

if (i < 4)
{
        if (r < turn_p)
        {
            angular = -0.3;
        }

        if (r < turn_p2)
        {
            angular = -0.75;
        }

        if (r < turn_p3)
        {
            linear = 0;
        }

}

else
{
        if (r < turn_p)
        {
            angular = 0.3;
        }

        if (r < turn_p2)
        {
            angular = 0.75;
        }

        if (r < turn_p3)
        {
            linear = 0;
        }
}
    }

if ((pointcloud->points[7].y > -turn_p) && (pointcloud->points[0].y < turn_p))
{
    angular = 0;
    linear = -0.2;
}


    msg.linear.x = linear;
    msg.angular.z = angular;
    pub_cmd_vel.publish(msg);
}


void mySigintHandler(int sig)
{
    printf("Caught exit signal\n");
    msg.linear.x = 0; msg.angular.z = 0; pub_cmd_vel.publish(msg);
    ros::shutdown();
}

int main (int argc , char** argv)
{
    ros::init(argc, argv, "prison_break");

    ros::NodeHandle n;
    ros::Rate rate(10);

// Register signal and signal handler
   signal(SIGINT, mySigintHandler);


    ros::Subscriber sensor_sub = n.subscribe<sensor_msgs::PointCloud>("/robin/sonar", 1, &sensor_callback);
    pub_cmd_vel = n.advertise<geometry_msgs::Twist>("/robin/cmd_vel",1);


    while(n.ok())
    {
        rate.sleep();

        ros::spin();
    }
}
